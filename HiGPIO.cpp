#include <stddef.h>
#include <stdio.h>
#include "stm32f10x.h"

USART_InitTypeDef USART_InitStructure;
GPIO_InitTypeDef GPIO_InitStructure;

int main(void){
    GPIO_InitStructure.GPIO_Pin=GPIO_Pin_13;
    GPIO_InitStructure.GPIO_Mode=GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
    while(1){
        /*Toggle LED which connected to PC13*/
        GPIOC->ODR^=GPIO_Pin_13;
        /*delay*/
        for(int i=1;i<0x100000;i++);
    }
}